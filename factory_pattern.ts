interface Phone {
  createPhone: () => void;
}
class IPhone implements Phone {
  createPhone() {
    console.log("IPhone is createPhone.");
  }
}
class Samsung implements Phone {
  createPhone() {
    console.log("Samsung is createPhone.");
  }
}

class MobileFactory {
  private constructor() {}
  static getMobile(mobileName: string): Phone | null {
    if (mobileName.toLowerCase() == "iphone") {
      return new IPhone();
    }
    if (mobileName.toLowerCase() == "samsung") {
      return new Samsung();
    }
    return null;
  }
}

let iPhone = MobileFactory.getMobile("samsunG");

iPhone?.createPhone();
