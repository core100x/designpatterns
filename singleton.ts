class Person {
  private constructor() {}

  private static personInstance?: Person;
  createCounter() {}
  static getIntance(): Person {
    if (!this.personInstance) {
      this.personInstance = new Person();
    }
    return this.personInstance;
  }
}
let instance = Person.getIntance();
let instance1 = Person.getIntance();
let instance2 = Person.getIntance();

console.log(instance == instance1);
console.log(instance1 == instance2);
