const data = [
  { name: "Abc", age: 20 },
  { name: "def", age: 21 },
  { name: "ghi", age: 22 },
  { name: "jkl", age: 24 },
  { name: "mno", age: 23 },
];

let _obj: any = {};
for (let index in data) {
  let item = data[index];
  if (_obj[index]) {
  } else {
    _obj[data[index].name] = data[index];
  }
}
console.log(_obj);
