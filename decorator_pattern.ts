abstract class Icecream {
  private description?: string;
  getDesc() {
    return this.description;
  }
  abstract cost(): number;
}

abstract class IceCreamDecorator extends Icecream {
  abstract cost(): number;
}

class Icecream1 extends Icecream {
  cost(): number {
    return 10;
  }
  getDesc(): string | undefined {
    return "IceCream1";
  }
}

class RainbowDecoration extends IceCreamDecorator {
  iceCream: Icecream;
  constructor(icecream: Icecream) {
    super();
    this.iceCream = icecream;
  }
  cost(): number {
    return this.iceCream.cost() + 20;
  }
  getDesc(): string | undefined {
    return "Rainbow Decoration Applied";
  }
}

let obj = new RainbowDecoration(new Icecream1());

console.log(obj.cost());
console.log(obj.getDesc());
