abstract class Phone {
  brandName: string = "";
  modelName: string = "";
  constructor(brandName: string, modelName: string) {
    this.brandName = brandName;
    this.modelName = modelName;
  }
  buildPhone() {
    console.log("Building Phone : ", this.brandName);
    console.log("Building Phone Model : ", this.modelName);
  }
}

class IPhone13 extends Phone {
  constructor(name: string) {
    super("Apple", name);
  }
}
class Samsung extends Phone {
  constructor() {
    super("Samsung", "S23");
  }
}

abstract class Mobile {
  abstract createMobile(modelName: string): Phone;
  buildPhone(modelName: string): Phone {
    let p = this.createMobile(modelName);
    p.buildPhone();
    return p;
  }
}

class IPhoneFactory extends Mobile {
  createMobile(modelName: string): Phone {
    return new IPhone13(modelName);
  }
}

let iphoneFac = new IPhoneFactory();
iphoneFac.buildPhone("Iphone 13 Pro");
