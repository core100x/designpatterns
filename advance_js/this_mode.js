// "use strict";
// console.log(window);
// function x() {
//   let self = this;
//   console.log(this);
// }

// console.log(globalThis);

let obj = {
  fn: "first name",
  printDetails: function () {
    console.log(this.fn);
  },
};

// let obj1 = {
//   fn: "Some Name",
// };
// obj.printDetails.bind(obj1);

// with arrow

let obj2 = {
  a: "name",
  printDetails: function () {
    console.log(this);
  },
};

obj2.printDetails();
