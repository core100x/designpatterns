const arr = [
  { fn: "aa", ln: "bb", age: 60 },
  { fn: "bb", ln: "bb", age: 80 },
  { fn: "cc", ln: "cc", age: 75 },
  { fn: "dd", ln: "dd", age: 45 },
  { fn: "dd", ln: "dd", age: 45 },
  { fn: "dd", ln: "dd", age: 45 },
  { fn: "dd2", ln: "dd2", age: 60 },
  { fn: "dd1", ln: "dd2", age: 75 },
];
// ---------------------------------------------------- EXAMPLE ONE
// get Sum
const numbers = [1, 2, 3, 4, 5];
// let initial = 0;
// for (let i of numbers) {
//   initial += i;
// }
// let getSum = numbers.reduce((acc, curr) => {
//   return (acc += curr);
// }, 0);
// console.log("sum : ", initial);
// console.log("sum (reduce) : ", getSum);
// ---------------------------------------------------- EXAMPLE TWO
// get MAX
const numbers1 = [1, 10, 2, 3, 4, 5];
let max = 0;
for (let i of numbers1) {
  if (max < i) max = i;
}
// console.log(max);
let getMax = numbers1.reduce((acc, curr) => {
  if (curr > acc) acc = curr;
  return acc;
}, 0);
// ----------------------------------------------------
// console.log(getMax);
// nested
const data = [
  { category: "A", value: 1 },
  { category: "B", value: 2 },
  { category: "A", value: 3 },
  { category: "A", value: 60 },
  { category: "A", value: 10 },
  { category: "B", value: 4 },
  { category: "B", value: 200 },
];

// let obj = {}; // { A : [1,3,60,10], B:[4,200,2] }
// for (let i in data) {
//   let cate = data[i].category;
//   let value = data[i].value;
//   if (obj[cate]) {
//     obj[cate].push(value);
//     //
//   } else {
//     obj[cate] = [value];
//   }
// }

// let count = {};
// for (let i in obj) {
//   if (!count[i]) {
//     count[i] = obj[i].length;
//   } else {
//     count[i] = 0;
//   }
// }

// let withReduce = data.reduce((acc, curr) => {
//   let cate = curr.category;
//   let val = curr.value;
//   let key = acc[cate];
//   key ? key.push(val) : (acc[cate] = [val]);
//   return acc;
// }, {});

// console.log(withReduce);

// ----------------------------------------------------
// console.log(getMax);
// nested
const users = [
  { category: "A", value: 10 },
  { category: "Z", value: 50 },
  { category: "B", value: 20 },
  { category: "C", value: 30 },
  { category: "D", value: 50 },
  { category: "E", value: 65 },
  { category: "F", value: 46 },
  { category: "G", value: 24 },
  { category: "H", value: 50 },
];

let userList = users.reduce((acc, curr) => {
  if (curr.value > 30) {
    if (acc.length) {
      acc.push(curr.category);
    } else {
      acc = [curr.category];
    }
  }
  return acc.sort();
}, []);

console.log(userList);
